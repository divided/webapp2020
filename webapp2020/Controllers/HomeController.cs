﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using webapp2020.Data;
using webapp2020.Models;
using Microsoft.AspNetCore.Identity;
using System;

namespace webapp2020.Controllers
{
    public class HomeController : Controller
    {
        private readonly AppDBContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public HomeController(AppDBContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/Account/Login", new { area = "Identity" });
            }

            await BuildDataForPage(_context.Post.ToList());

            return View();
        }

        public async Task<IActionResult> FilterPosts(long? interestId, string userId)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/Account/Login", new { area = "Identity" });
            }

            List<Post> filteredPosts = _context.Post.ToList();

            if (interestId.HasValue)
            {
                filteredPosts = _context.Post.Where(t => t.InterestId == interestId).ToList();
            }

            if (userId != null)
            {
                filteredPosts = filteredPosts.Where(t => t.UserId == userId).ToList();
            }

            await BuildDataForPage(filteredPosts);

            return View("Index");
        }

        public async Task BuildDataForPage(List<Post> postlist)
        {
            ViewBag.Posts = postlist;
            ViewBag.Posts.Reverse();
            ViewBag.IsFirstPostRecommended = PutRecommendedPostAtStart(ViewBag.Posts);

            List<Interest> interests = _context.Interest.ToList();

            var usersToPosts = new List<IdentityUser>();
            var interestsToPosts = new List<Interest>();
            var likesToPost = new List<int>();
            var isLikePost = new List<bool>();

            foreach (Post post in ViewBag.Posts)
            {
                usersToPosts.Add(await _userManager.FindByIdAsync(post.UserId));
                likesToPost.Add(_context.Like.Count(g => g.PostId == post.Id));
                isLikePost.Add(_context.Like.Any(g => (g.PostId == post.Id &&
                                                       g.UserId == _userManager.GetUserId(User))));

                if (post.InterestId == 0)
                {
                    interestsToPosts.Add(null);
                }
                else
                {
                    interestsToPosts.Add(interests.Find(interest => interest.Id == post.InterestId));
                }
            }

            ViewBag.UsersToPosts = usersToPosts;
            ViewBag.Interest = interests;
            ViewBag.InterestsToPosts = interestsToPosts;
            ViewBag.likesToPost = likesToPost;
            ViewBag.isLikePost = isLikePost;
            ViewBag.Users = _context.Users.ToList();
        }

        private bool PutRecommendedPostAtStart(List<Post> posts)
        {
            // No post is recommended if there are not enough posts in total
            if (posts.Count() <= 3) {
                return false;
            }

            // Gets a list of all posts that are liked by the user
            string userId = _userManager.GetUserId(User);
            var userLikes = from l in _context.Like
                            where l.UserId == userId
                            select l;

            // All posts liked by the user
            List<Post> postsLikedByUser = (from p in posts
                                           join ul in userLikes on p.Id equals ul.PostId
                                           select p).ToList();

            // Find all liked interests by the user
            Dictionary<long, int> likedInterests = new Dictionary<long, int>();
            foreach (var interest in _context.Interest) 
            {
                likedInterests.Add(interest.Id, 0);
            }
            foreach (var likedPost in postsLikedByUser)
            {
                if (likedPost.InterestId != 0)
                {
                    likedInterests[likedPost.InterestId] += 1;
                }
            }

            // Algorithm constants for evaluation
            double OLD_POST_EVALUATION_PENALTY = 0.05;
            double LIKED_BY_USER_BONUS = 0.4;
            double INTEREST_BONUS = 0.15;

            double maxEvaluation = Double.MinValue;
            int bestPostIndex = -1;

            // Evaluate each posts based on user likes and his most liked interests and post time
            for (int postIndex = 0; postIndex < posts.Count(); postIndex++)
            {
                double currentEvaluation = 0;

                Post post = posts[postIndex];
                bool isPostLikedByUser = postsLikedByUser.Any(p => p.UserId == userId);

                // Apply penalty for old posts
                currentEvaluation -= postIndex * OLD_POST_EVALUATION_PENALTY;

                // Add bonus if post is liked by user
                if (isPostLikedByUser) {
                    currentEvaluation += LIKED_BY_USER_BONUS;
                }

                // Add bonus if same post interest was liked many times by the user
                if (post.InterestId != 0) {
                    currentEvaluation += likedInterests[post.InterestId] * INTEREST_BONUS;
                }

                // If we find better evaluation, then we replace the best one with it
                if (currentEvaluation > maxEvaluation)
                {
                    maxEvaluation = currentEvaluation;
                    bestPostIndex = postIndex;
                }
            }

            // Put best evaluated post at the start of the posts list
            var temp = posts[0];
            posts[0] = posts[bestPostIndex];
            posts[bestPostIndex] = temp;

            return true;
        }

        public IActionResult FavoritePlaces()
        {
            return View();
        }

        public IActionResult OurStory()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

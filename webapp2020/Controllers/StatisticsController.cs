﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using webapp2020.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;


namespace webapp2020.Controllers
{
    public class StatisticsController : Controller
    {
        private const int MAX_USERS_TO_POSTS_COUNT = 5;
        private readonly AppDBContext _context;

        public StatisticsController(AppDBContext context)
        {
            _context = context;
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            return View();
        }
        

        [HttpGet]
        public IEnumerable<UserToPostCount> GetTopUserToPostCounts()
        {
            var UsersToPosts = from u in _context.Users
                               join p in _context.Post on u.Id equals p.UserId
                               select new { Username = u.UserName, PostId = p.Id };

            var UsersToPostCounts = from userToPost in UsersToPosts
                                    group userToPost by userToPost.Username into k
                                    select new UserToPostCount() { NumberOfPosts = k.Count(), Username = k.Key };

            return (from userToPostCount in UsersToPostCounts
                    orderby userToPostCount.NumberOfPosts descending
                    select userToPostCount).Take(MAX_USERS_TO_POSTS_COUNT)
                    .ToList();
        }

        [HttpGet]
        public IEnumerable<InterestToLikeCount> GetInterestsToLikesCounts() 
        {
            var AllPostInterestsWithLikes = from l in _context.Like
                               join p in _context.Post on l.PostId equals p.Id
                               select new { Id = p.InterestId };

            var InterestsIdsToLikeCounts = from InterestWithLike in AllPostInterestsWithLikes
                                           group InterestWithLike by InterestWithLike.Id into t
                                           select new { NumberOfInterestLikes = t.Count(), Id = t.Key };

            List<InterestToLikeCount> InterestToLikes = new List<InterestToLikeCount>();
            foreach (var interestIdToLikeCount in InterestsIdsToLikeCounts)
            {
                InterestToLikes.Add(new InterestToLikeCount()
                {
                    InterestName = GetInterestNameById(_context, interestIdToLikeCount.Id),
                    LikeCount = interestIdToLikeCount.NumberOfInterestLikes
                });
            }

            return InterestToLikes;
        }

        private string GetInterestNameById(AppDBContext context, long InterestId)
        {
            if (InterestId == 0) 
            {
                return "No interest";
            }

            return (from i in _context.Interest
                    where i.Id == InterestId
                    select i.Name).FirstOrDefault();
        }

        public class UserToPostCount
        {
            public String Username { get; set; }
            public int NumberOfPosts { get; set; }
        }


        public class InterestToLikeCount 
        {
            public String InterestName { get; set; }
            public int LikeCount { get; set; }
        }
    }
}

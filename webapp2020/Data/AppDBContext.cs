﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using webapp2020.Models;

namespace webapp2020.Data
{
    public class AppDBContext : IdentityDbContext<IdentityUser>
    {
        public DbSet<Interest> Interest { get; set; }
        public DbSet<Post> Post { get; set; }

        public DbSet<Like> Like { get; set; }

        public AppDBContext (DbContextOptions<AppDBContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<webapp2020.Models.Location> Location { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Hosting;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using webapp2020.Models;
using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Threading.Tasks;

namespace webapp2020.Data
{
    public static class DbSeeder
    {
        public async static Task SeedDB(IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                AppDBContext context = services.GetRequiredService<AppDBContext>();

                SeedRoles(context);
                SeedLocations(context);
                SeedInterests(context);
                await SeedUsers(context);
            }
        }

        private static void SeedInterests(AppDBContext context)
        {
            if (!context.Interest.Any())
            {
                String[] Interests = new String[] { "Sports",
                                                    "Art",
                                                    "Programming",
                                                    "Politics",
                                                    "Music",
                                                    "Food",
                                                    "dank memes",
                                                    "Block chain"};

                for (int interestIndex = 0; interestIndex < Interests.Length; interestIndex++)
                {
                    String interest = Interests[interestIndex];
                    context.Interest.Add(new Interest() { Name = interest });
                }
            }

            context.SaveChanges();
        }

        private static void SeedRoles(AppDBContext context)
        {
            if (!context.Roles.Any())
            {
                String[] roles = new String[] { "Admin", "User" };

                for (int roleIndex = 0; roleIndex < roles.Length; roleIndex++)
                {
                    String role = roles[roleIndex];
                    context.Roles.Add(new IdentityRole(role) { Id = (roleIndex + 1).ToString(), NormalizedName = role });
                }
            }
        }


        private static void SeedLocations(AppDBContext context)
        {
            if (!context.Location.Any()) 
            {
                context.Location.Add(new Location() { Name= "מכללה למנהל", Latitude = 31.9697346, Longitude = 34.7720053, Description = "Our favorite college" });
                context.Location.Add(new Location() { Name = "ימית 2000", Latitude = 32.0049492, Longitude = 34.790819, Description = "Attraction for the whole family" });
                context.Location.Add(new Location() { Name = "בסיס צריפין", Latitude = 31.9573365, Longitude = 34.8361779, Description = "Happiest place on earth" });
            }

            context.SaveChanges();
        }


        private async static Task SeedUsers(AppDBContext context)
        {
            var user = new ApplicationUser
            {
                UserName = "admin",
                Email = "admin@gmail.com",
                NormalizedUserName = "ADMIN",
            };

            if (!context.Users.Any(u => u.UserName == user.UserName))
            {
                var password = new PasswordHasher<ApplicationUser>();
                var hashed = password.HashPassword(user, "admin");
                user.PasswordHash = hashed;
                var userStore = new UserStore<ApplicationUser>(context);
                await userStore.CreateAsync(user);
                await userStore.AddToRoleAsync(user, "Admin");
            }

            context.SaveChanges();
        }
    }

    public class ApplicationUser : IdentityUser
    {

    }
}

using System;
using webapp2020.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(webapp2020.Areas.Identity.IdentityHostingStartup))]
namespace webapp2020.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<AppDBContext>(options =>
                    options.UseSqlServer(context.Configuration.GetConnectionString("AppDBContextConnection")));
            });
        }
    }
}
﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace webapp2020.Middlewares
{
    public class NotFoundMiddleware
    {
        private readonly RequestDelegate _next;

        public NotFoundMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            context.Response.Redirect("/");
        }
    }
}

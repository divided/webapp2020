﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace webapp2020.Migrations
{
    public partial class addInterestsToPost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "PostId",
                table: "Interest",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Interest_PostId",
                table: "Interest",
                column: "PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_Interest_Post_PostId",
                table: "Interest",
                column: "PostId",
                principalTable: "Post",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Interest_Post_PostId",
                table: "Interest");

            migrationBuilder.DropIndex(
                name: "IX_Interest_PostId",
                table: "Interest");

            migrationBuilder.DropColumn(
                name: "PostId",
                table: "Interest");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace webapp2020.Migrations
{
    public partial class fixedPostModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Interest_Post_PostId",
                table: "Interest");

            migrationBuilder.DropIndex(
                name: "IX_Interest_PostId",
                table: "Interest");

            migrationBuilder.DropColumn(
                name: "PostId",
                table: "Interest");

            migrationBuilder.AddColumn<string>(
                name: "InterestId",
                table: "Post",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InterestId",
                table: "Post");

            migrationBuilder.AddColumn<long>(
                name: "PostId",
                table: "Interest",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Interest_PostId",
                table: "Interest",
                column: "PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_Interest_Post_PostId",
                table: "Interest",
                column: "PostId",
                principalTable: "Post",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

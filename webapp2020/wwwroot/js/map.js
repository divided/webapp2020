﻿function loadMap() {
    const map = new Microsoft.Maps.Map(document.getElementById('map'), {
        credentials: 'Aq4WZM_BGIqJgI2vO1tgoWyHR-oZ8iYaZDHcV6fiTg0m8JMgTUIVQzb74kYHL0bS',
    });

    map.setView({
        mapTypeId: Microsoft.Maps.MapTypeId.road,
        center: new Microsoft.Maps.Location(32.100000, 34.788052),
        zoom: 10
    });

    $.ajax({
        type: 'GET',
        url: '/Locations/All',
        async: true,
        success: function (locations) {
            locations.forEach(location => addLocationToMap(map, location));
        },
        error: function (response) {
            alert("Error on getting all locations");
            console.log(response);
        }
    });
}

const addLocationToMap = (map, place) => {
    const pushPin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(place.latitude, place.longitude), { color: 'blue' });
    pushPin.setOptions({ enableHoverStyle: true });
    pushPin.metadata = {
        id: place.name,
        title: place.name,
        description: place.description
    };

    map.entities.push(pushPin);

    const infoboxTemplate = '<div class="customInfobox"><div class="infoboxTitle">{title}</div>{description}</div>';

    const infobox = new Microsoft.Maps.Infobox(pushPin.getLocation(), {
        visible: false
    });

    infobox.setMap(map);

    const showInfobox = e => {
        if (e.target.metadata) {
            infobox.setOptions({
                location: e.target.getLocation(),
                htmlContent: infoboxTemplate.replace('{title}', e.target.metadata.title).replace('{description}', e.target.metadata.description),
                visible: true
            });
        }
    }
    const hideInfobox = e => {
        infobox.setOptions({ visible: false });
    }


    Microsoft.Maps.Events.addHandler(pushPin, 'mouseover', showInfobox);
    Microsoft.Maps.Events.addHandler(pushPin, 'mouseout', hideInfobox);
};

﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$(document).ready(function () {
    loadNews();
    drawCanvas();
})

function drawCanvas() {
    var canvas = document.getElementById("starkCanvas");
    var ctx = canvas.getContext("2d");

    ctx.font = "30px Verdana";
    var gradient = ctx.createLinearGradient(0, 0, canvas.width, 0);
    gradient.addColorStop("0", " magenta");
    gradient.addColorStop("0.5", "pink");
    gradient.addColorStop("1.0", "cyan");
    ctx.fillStyle = gradient;
    ctx.fillText("STARK", 10, 50);
}

function loadNews() {
    var url = 'https://newsapi.org/v2/top-headlines?country=il&' +
        'apiKey=4f1b487c72494283aa1fb019067cfb6d';

    $.getJSON(url).then(function (res) {
        res['articles'].forEach((article) => {
            const newsDiv = document.getElementById('News');
            const card = document.createElement('div')
            card.className = 'card border-light mb-3';

            var image = document.createElement('img');
            image.className = 'card-img-top';

            if (article.urlToImage == null) {
                image.setAttribute('src', 'news.png');
                image.className = 'card-img-top news';
            } else {
                image.setAttribute('src', article.urlToImage);
                image.className = 'card-img-top';
            }

            var title = document.createElement('div');
            title.className = 'card-header';
            title.innerHTML = article.title;
            var footer = document.createElement('div');
            footer.class = 'card-footer';

            var a = document.createElement('a');
            a.className = 'btn btn-light btn-sm news';
            a.innerHTML = 'Read More';
            a.setAttribute('href', article.url);
            a.setAttribute('target', '_blank');
            footer.appendChild(a);

            var date = document.createElement('aside');
            date.innerHTML = new Date(article.publishedAt).toDateString();

            newsDiv.appendChild(card);
            title.appendChild(image);
            card.appendChild(title);
            
            if (article.description != null && article.description !== "0") {
                var body = document.createElement('div');
                body.className = 'card-body';
                body.innerHTML = article.description;
                card.appendChild(body);
            }

            card.appendChild(date);
            card.appendChild(footer);
        })
    })
}

﻿function loadInfo(PostId, postIndex) {
    $.ajax({
        type: 'POST',
        url: "/Likes/LikesCountByPost",
        data: { postId: PostId },
        async: true,
        success: function (response) {
            $(`#numOfLikes-${postIndex}`).html(response);
        },
        error: function (response) {
            console.log("Error on getting likes count by post");
            console.log(response);
        }
    })
}

function setInterestId(id) {
    $('#InterestHiddenInput').val(id);
}

function increaseLikes(PostId, postIndex) {
    $.ajax({
        type: 'POST',
        url: '/Likes/Create',
        async: true,
        data: { PostId: PostId },
        success: function (response) {
            $(`#numOfLikes-${postIndex}`).html(loadInfo(PostId, postIndex));
            $(`#likeButton-${postIndex}`).removeClass("btn-light");
            $(`#likeButton-${postIndex}`).addClass("btn-primary");
        },
        error: function (response) {
            console.log("Error on like creation");
            console.log(response);
        }
    })
}

function deleteLike(PostId, postIndex) {
    $.ajax({
        type: 'POST',
        url: '/Likes/DeleteByPostId',
        data: { PostId: PostId },
        async: true,
        success: function (response) {
            $(`#numOfLikes-${postIndex}`).html(loadInfo(PostId, postIndex));
            $(`#likeButton-${postIndex}`).removeClass("btn-primary");
            $(`#likeButton-${postIndex}`).addClass("btn-light");
        },
        error: function (response) {
            console.log("Error on like deletion");
            console.log(response);
        }
    })
}

function isLikedThePost(PostId, postIndex) {
    $.ajax({
        type: 'POST',
        url: '/Likes/IsLikedPost',
        async: true,
        data: { PostId: PostId },
        success: function (response) {
            response ? deleteLike(PostId, postIndex) :
                       increaseLikes(PostId, postIndex);
        },
        error: function (response) {
            alert("Error on checking post like");
        }
    })
}

function fillEditPostDetails(PostId, Content, UserId) {
    $("#editPostId").val(PostId);
    $("#editContent").val(Content);
    $("#editUserId").val(UserId);
}

function validatePostForm() {
    var content = document.forms["postForm"]["content"].value;
    if (content == "") {
        alert("Content must be filled out");
        return false;
    }
}
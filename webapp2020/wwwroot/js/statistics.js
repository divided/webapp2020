﻿$.get("/Statistics/GetTopUserToPostCounts", data => {
    // set the dimensions and margins of the graph
    const margin = { top: 30, right: 30, bottom: 70, left: 60 },
        width = 600 - margin.left - margin.right,
        height = 600 - margin.top - margin.bottom;

    // append the svg object to the body of the page
    const svg = d3.select("#mostPostsChart")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    // X axis
    var x = d3.scaleBand()
        .range([0, width])
        .domain(data.map(function (userToPostCount) { return userToPostCount.username; }))
        .padding(0.2);
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x))
        .selectAll("text")
        .attr("transform", "translate(-10,0)rotate(-45)")
        .style("text-anchor", "end");

    let max = 0;
    for (var userToPostCount of data) {
        if (userToPostCount.numberOfPosts > max) {
            max = userToPostCount.numberOfPosts;
        }
    }
    max = max + (10 - (max % 10));

    // Add Y axis
    var y = d3.scaleLinear()
        .domain([0, max])
        .rangeRound([height, 0]);
    svg.append("g").call(d3.axisLeft(y));

    // Bars
    svg.selectAll("mybar")
        .data(data)
        .enter()
        .append("rect")
        .attr("x", function (d) { return x(d.username); })
        .attr("y", function (d) { return y(d.numberOfPosts); })
        .attr("width", x.bandwidth())
        .attr("height", function (d) { return height - y(d.numberOfPosts); })
        .attr("fill", "#3c96cf")
});


$.get("/Statistics/GetInterestsToLikesCounts", interestsToLikeCounts => {
    const data = interestsToLikeCounts
        .filter(interestToLikeCount => interestToLikeCount.likeCount > 0)
        .map(interestToLikeCount => {
        return {
            name: interestToLikeCount.interestName,
            value: interestToLikeCount.likeCount
        }
        });
    console.log(data);
    const width = 900;
    const height = 500;
    const pie = d3.pie()
        .sort(null)
        .value(d => d.value);

    const arcLabel = () => {
        const radius = Math.min(width, height) / 2 * 0.8;
        return d3.arc().innerRadius(radius).outerRadius(radius);
    };
    const color = d3.scaleOrdinal()
        .domain(data.map(d => d.name))
        .range(d3.quantize(t => d3.interpolateSpectral(t * 0.8 + 0.1), data.length + 5).reverse());

    const arc = d3.arc()
        .innerRadius(0)
        .outerRadius(Math.min(width, height) / 2 - 1);

    const arcs = pie(data);
    const svg = d3.select("#topInterestsByLikes")
                    .append("svg")
                    .attr("viewBox", [-width / 2, -height / 2, width, height]);

    svg.append("g")
        .attr("stroke", "white")
        .selectAll("path")
        .data(arcs)
        .join("path")
        .attr("fill", d => color(d.data.name))
        .attr("d", arc)
        .append("title")
        .text(d => `${d.data.name}: ${d.data.value.toLocaleString()}`);

    svg.append("g")
        .attr("font-family", "sans-serif")
        .attr("font-size", 12)
        .attr("text-anchor", "middle")
        .selectAll("text")
        .data(arcs)
        .join("text")
        .attr("transform", (d, i) => `translate(${arcLabel().centroid(d, i)})`)
        .call(text => text.append("tspan")
            .attr("y", "-0.4em")
            .attr("font-weight", "bold")
            .text(d => d.data.name))
        .call(text => text.filter(d => (d.endAngle - d.startAngle) > 0.25).append("tspan")
            .attr("x", 0)
            .attr("y", "0.7em")
            .attr("fill-opacity", 0.7)
            .text(d => d.data.value.toLocaleString()));
});
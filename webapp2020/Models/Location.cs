﻿using System.Diagnostics.CodeAnalysis;

namespace webapp2020.Models
{
    public class Location
    {
        public int Id { get; set; }

        [NotNull]
        public string Name { get; set; }
        
        public string Description { get; set; }

        [NotNull]
        public double Latitude { get; set; }

        [NotNull]
        public double Longitude { get; set; }
    }
}

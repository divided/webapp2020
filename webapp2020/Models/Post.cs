﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace webapp2020.Models
{
    public class Post
    {
        public long Id { get; set; }

        [NotNull]
        [MaxLength(360)]
        public string Content { get; set; }
        public ICollection<Like> Likes { get; set; }
        public long InterestId { get; set; }
        public String UserId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace webapp2020.Models
{
    public class Like
    {
        public int Id { get; set; }

        public long PostId { get; set; }

        public String UserId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace webapp2020.Models
{
   public class Interest
    {
        public int Id { get; set; }

        [NotNull]
        public String Name { get; set; }
    }
}
